import fetchData from './fetchData.js';
import { api} from './api.js';
import { addImg, createGallery } from './list.js';
import { search_fun, search } from './search.js';

//Получаем фотки из инсты для выведения постов
async function app() {
	const getPosts = async () => {
		const data = await fetchData(api.makeUrl());
		createGallery(data);
	}
	getPosts();
}
await app();

//Генерируем имена юзеров
function storiesGenerate() {
	const get = () =>{
		//const story = fetchData(random_stories_img.makeUrl());
		const userName = fetchData('./usernames.json');
		userName.then(data => writeHTML(data))
	}
	get();
}
storiesGenerate();

//Добавляем истории с рандомными аватарками и юзерами
function cloneStories(count) {
	let names = JSON.parse(localStorage.getItem('data')); //Распарсить объект
	let names_lengths = Object.keys(names).length; //Узнает длину объекта
	const stories = document.querySelector(".swiper");

	for (let index = 0; index < count; index++) {
		const newStories = stories.cloneNode(true)

		newStories.querySelector('.user_nickname').innerText = `@${names[
			Math.floor(Math.random() * names_lengths)]}`

		document.querySelector(".swiper-wrapper").appendChild(newStories);
	}
}
cloneStories(25);


//Записываем в localStorage имена юзеров
function writeHTML(str) {
	localStorage.setItem("data",JSON.stringify(str))
}

//Создание нового поста
const dropZone = document.querySelector('.new_post');
const input = document.querySelector('#newPost');
let file;

document.addEventListener('dragover', ev => ev.preventDefault())
document.addEventListener('drop', ev => ev.preventDefault())

dropZone.addEventListener('drop', ev => {
	ev.preventDefault()
	file = ev.dataTransfer.files[0]
	handleFile(file)
})

dropZone.addEventListener('click', () => {
	input.addEventListener('change', () => {
		file = input.files[0]
		handleFile(file)
	})
	input.click();
})

const handleFile = file => {
	console.log("dsd");
	if (file.type === 'text/html' ||
		file.type === 'text/css' ||
		file.type === 'text/javascript')
		return;
	// нас интересует то, что находится до слеша
	const type = file.type.replace(/\/.+/, '')
	// проверяем
	createImage(file);
}

const createImage = image => {
	let img_url = URL.createObjectURL(image)
	localStorage.setItem("added_post", img_url);
	addImg(localStorage.getItem("added_post"));
	// удаляем ссылку на файл
	URL.revokeObjectURL(image)
}


//Перекючение на страницу камеры
let camera = document.querySelector('.camera');
camera.addEventListener('click', (ev) => {
	ev.preventDefault();
	//window.location.replace('camera.html')
})

//Выведение предупреждения
let onWork = document.querySelectorAll('#onWork');
for (const elem of onWork) {
	elem.addEventListener('click', (event) => {
		event.preventDefault();
		//МОЖЕТЕ ПОПРОБОВАТЬ СНЯТЬ КОММЕНТ
		// Swal.fire({
		// 	title: 'В работе',
		// 	width: 600,
		// 	padding: '15em',
		// 	color: '#716add',
		// 	background: 'url("../borya.gif") no-repeat',
		// })
		if (window.innerWidth > 900) {
			// Swal.fire({
			// 	title: 'В разработке',
			// 	width: 800,
			// 	padding: '8em',
			// 	color: '#716add',
			// 	background: 'url("../insta.gif")',
			// })
			Swal.fire({
			title: 'В работе',
			width: 600,
			padding: '15em',
			color: '#716add',
			background: 'url("../borya.gif") no-repeat',
		})
		} else {
			Swal.fire({
				title: 'В разработке',
				width: 300,
				padding: '5em',
				color: '#716add',
				background: 'url("../insta.gif")',
			})
		}
	})
}

//Функционал ЛАЙКОВ
document.addEventListener("DOMContentLoaded", async()=>{
	function likes() {
		document.querySelectorAll('.unfilled').forEach(
			eachUnfilledBtn => {
				eachUnfilledBtn.addEventListener('click', event => {
					const filledBtn = eachUnfilledBtn.parentNode.children.item(1);
					if (filledBtn) {
						event.currentTarget.setAttribute('style', 'display: none')
						filledBtn.removeAttribute('style');
					}
				})
			}
		)

		document.querySelectorAll('.filled').forEach(
			eachFilledBtn => {
				eachFilledBtn.addEventListener('click', event => {
					const unfilledBtn = eachFilledBtn.parentNode.children.item(0);
					if (unfilledBtn) {
						event.currentTarget.setAttribute('style', 'display: none');
						unfilledBtn.removeAttribute('style');
					}
				})
			}
		)
	}
	setTimeout(likes, 1000);
})



// if (likedBtn) {
// 	likedBtn.addEventListener('click',()=>{
// 		unlikedBtn.setAttribute('style','display:none');
// 		likedBtn.removeAttribute('style');
// 	})
// }




















