import fetchData from './fetchData.js';
import { random_img_api } from './api.js';
import { createImg } from './list.js';
import {search_fun, search} from './search.js';

document.addEventListener("DOMContentLoaded", async () => {
		function addImg(img_url) {
			let body = document.querySelector('.publications');
			const galleryBlock = body.querySelector('.post');

			const post = galleryBlock.cloneNode(true); // клонируем в остальные и меняем src
			post.removeAttribute('id');
			post.querySelector('img').src = img_url;
			body.querySelector('.posts').appendChild(post)
		}

		async function random_img() {
			let url = random_img_api.makeUrl(localStorage.getItem('searchTag'));
			const getPublication = async () => {
				const data = await fetchData(url);
				const img_array = createImg(data);
				img_array.forEach(element => {
					console.log(element);
					addImg(element)
				});
			}
			getPublication();
		}
		await random_img();

		let title = document.querySelector('.title');
		let searchTag = localStorage.getItem('searchTag');
		title.innerHTML = `#${searchTag}`;
	
})