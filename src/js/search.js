let main = document.querySelector('.logo');
main.addEventListener('click', () => {
	window.location.replace("index.html");
})

export let search = document.querySelector('.search');
export function search_fun(event) {
	event.preventDefault();
	let searchTag = search.lastElementChild.value;
	localStorage.setItem("searchTag", searchTag);
	if(searchTag) window.location.replace("search_posts.html");
}

search.firstElementChild.addEventListener('click', (event) => {
	search_fun(event);
})