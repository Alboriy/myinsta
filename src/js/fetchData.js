export default async function fetchData(url){
	const data = await fetch(url); 
	try {
		if(data.ok) {
			return data.json();
		}
	}catch(e) {
		throw new Error(e)
	}
}