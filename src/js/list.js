export function createGallery({ data }) {
	const galleryBlock = document.querySelector('.post');
	data.forEach(({ media_url }) => {
		const post = galleryBlock.cloneNode(true);
		post.removeAttribute('id');
		post.querySelector('img').src = media_url;
		document.querySelector('.posts').appendChild(post)
	});
}

export function createImg(data) {
	let array = [];
	try {
		for (let index = 0; index < 10; index++) {
			array.push(data.results[index].urls.regular);
		}
		return array;	
	} catch (error) {
		alert('Нет доступных изображений!')
	}
}

export function addImg(img_url){
	const galleryBlock = document.querySelector('.post');
	const post = galleryBlock.cloneNode(true);
	post.removeAttribute('id');
	post.querySelector('#img').src = img_url;
	document.querySelector('.posts').appendChild(post);
}